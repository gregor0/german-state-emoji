# German state emoji

I emojis for the sixteen states of Germany. They are somewhere in between the coat of arms and the flags, depending of what would be recognizable. The colors are right, but that might look a bit too colorfull in some contexts. These are based on public domain images. I used the official [Unicode Subdivision Codes](https://www.unicode.org/cldr/charts/34/supplemental/territory_subdivisions.html) for naming. The resized version (1000px) is suitable for discord, all files are below 256kb. 

![Preview of the emojis, each is 32 pixels](preview_32x32_each.png "They look like this if they are 32px")
![Preview of the emojis, each is 64 pixels](preview_64x64_each.png "They look like this if they are 64px")

## Todo:
* Make more sizes
* Is bremen okay like this?
